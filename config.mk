# cuentas version
VERSION = 1.0

# Paths
SCRIPTSDIR = /etc/cuentas
PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

# Include and libs
LIBS = -lpqxx

# Flafs
CFLAGS = -march=native -std=c++17 -Wall ${LIBS}

# Compiler
CC = g++
