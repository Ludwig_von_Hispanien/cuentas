#ifndef __BASE_DE_DATOS__HH__
#define __BASE_DE_DATOS__HH__


#include <stdio.h>
#include <string>
#include <pqxx/pqxx>

#include "Fecha.hh"


class BaseDeDatos
{
public:
  BaseDeDatos();
  virtual ~BaseDeDatos();

  void ingresar(std::string sig);

  pqxx::result dinero();
  
  pqxx::result dia();
  pqxx::result mes();
  pqxx::result ano();

  pqxx::result dia(std::string nau);
  pqxx::result mes(std::string nau);
  pqxx::result ano(std::string nau);

  
protected:
  Fecha *fecha;
};

BaseDeDatos::BaseDeDatos()
{
  fecha = new Fecha();
}

BaseDeDatos::~BaseDeDatos()
{
  delete fecha;
}

void BaseDeDatos::ingresar(std::string sig)
{
  
  std::string nau = " SELECT actualizar ('";
  nau += std::to_string(fecha->getAno()) + "-";
  nau += std::to_string(fecha->getMes()) + "-";
  nau += std::to_string(fecha->getDia());
  nau += "'," + sig + ")";

  pqxx::connection con("dbname=cuentas user=cuentas_usuario password=cuentaspass");
  pqxx::work wrk(con);
  pqxx::result res = wrk.exec(nau);
  
  wrk.commit();
  
}

pqxx::result BaseDeDatos::dinero()
{

  pqxx::connection con("dbname=cuentas user=cuentas_usuario password=cuentaspass");
  pqxx::work wrk(con);
  pqxx::result res = wrk.exec("SELECT * FROM dinero");

  wrk.commit();
  
  return res;

}

pqxx::result BaseDeDatos::dia()
{

  std::string fech = "";
  fech += std::to_string(fecha->getAno()) + "-";
  fech += std::to_string(fecha->getMes()) + "-";
  
  pqxx::connection con("dbname=cuentas user=cuentas_usuario password=cuentaspass");
  pqxx::work wrk(con);
  pqxx::result res = wrk.exec("SELECT * FROM transacciones_dia WHERE fecha >= '" + fech + "01'");

  wrk.commit();
  
  return res;

}

pqxx::result BaseDeDatos::mes()
{

  std::string fech = "";
  fech += std::to_string(fecha->getAno()) + "-";
  fech += std::to_string(fecha->getMes()) + "-";
  
  pqxx::connection con("dbname=cuentas user=cuentas_usuario password=cuentaspass");
  pqxx::work wrk(con);
  pqxx::result res = wrk.exec("SELECT * FROM transacciones_mes WHERE fecha >= '" + fech + "01'");

  wrk.commit();

  return res;

}

pqxx::result BaseDeDatos::ano()
{

  std::string fech = "";
  fech += std::to_string(fecha->getAno()) + "-";
  
  pqxx::connection con("dbname=cuentas user=cuentas_usuario password=cuentaspass");
  pqxx::work wrk(con);
  pqxx::result res = wrk.exec("SELECT * FROM transacciones_ano WHERE fecha BETWEEN '" + fech + "01-01' AND '" + fech + "12-31'");

  wrk.commit();

  return res;

}

pqxx::result BaseDeDatos::dia(std::string nau)
{

  pqxx::connection con("dbname=cuentas user=cuentas_usuario password=cuentaspass");
  pqxx::work wrk(con);
  pqxx::result res = wrk.exec("SELECT * FROM transacciones_dia WHERE fecha = " + nau );

  wrk.commit();
  
  return res;

}

pqxx::result BaseDeDatos::mes(std::string nau)
{
  
  pqxx::connection con("dbname=cuentas user=cuentas_usuario password=cuentaspass");
  pqxx::work wrk(con);
  pqxx::result res = wrk.exec("SELECT * FROM transacciones_mes WHERE fecha BETWEEN '" + nau + "01' AND '" + nau + "31'");

  wrk.commit();

  return res;

}

pqxx::result BaseDeDatos::ano(std::string nau)
{

  pqxx::connection con("dbname=cuentas user=cuentas_usuario password=cuentaspass");
  pqxx::work wrk(con);
  pqxx::result res = wrk.exec("SELECT * FROM transacciones_ano WHERE fecha BETWEEN '" + nau + "01-01' AND '" + nau + "12-31'");

  wrk.commit();

  return res;

}

#endif
