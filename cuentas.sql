---
---	 BASE DE DATOS - CUENTAS
---
--- BY: Ludwig von Hispanien
---
---
---
--- INICIALIZACION
---

\c cuentas cuentas_usuario

DROP TABLE IF EXISTS transacciones_dia;
DROP TABLE IF EXISTS transacciones_mes;
DROP TABLE IF EXISTS transacciones_ano;
DROP TABLE IF EXISTS dinero;
DROP FUNCTION IF EXISTS actualizar;

---
--- CREACION DE TABLA
---

CREATE TABLE transacciones_dia ( fecha DATE , tipo VARCHAR(20) , detalles VARCHAR(20) , dinero DECIMAL(8,2) , comentario VARCHAR(20) );
CREATE TABLE transacciones_mes ( fecha DATE , ingreso_envio DECIMAL(8,2) , ingreso_trabajo DECIMAL(8,2) , ingreso_otros DECIMAL(8,2) , ingreso_total DECIMAL(8,2) ,
       	     		       	       	      movimiento_deposito DECIMAL(8,2) , movimiento_retiro DECIMAL(8,2) ,
					      egreso_hacienda DECIMAL(8,2) , egreso_alimentos DECIMAL(8,2) , egreso_transporte DECIMAL(8,2) ,
					      egreso_necesidad DECIMAL(8,2) , egreso_otros DECIMAL(8,2) , egreso_total DECIMAL(8,2) , balance DECIMAL(8,2) );
CREATE TABLE transacciones_ano ( fecha DATE , ingreso_envio DECIMAL(8,2) , ingreso_trabajo DECIMAL(8,2) , ingreso_otros DECIMAL(8,2) , ingreso_total DECIMAL(8,2) ,
       	     		       	       	      movimiento_deposito DECIMAL(8,2) , movimiento_retiro DECIMAL(8,2) ,
					      egreso_hacienda DECIMAL(8,2) , egreso_alimentos DECIMAL(8,2) , egreso_transporte DECIMAL(8,2) ,
					      egreso_necesidad DECIMAL(8,2) , egreso_otros DECIMAL(8,2) , egreso_total DECIMAL(8,2) , balance DECIMAL(8,2) );
CREATE TABLE dinero ( tipo VARCHAR(20) , dinero DECIMAL(8,2) );

---
--- INICIALIZACION DE TABLA
---

INSERT INTO dinero VALUES ( 'cuenta_bancaria' , 0.0 );
INSERT INTO dinero VALUES ( 'efectivo' , 0.0 );

---
--- UPDATE FUNCTION
---

CREATE FUNCTION actualizar ( fecha_t DATE , tipo_t VARCHAR , detalles_t VARCHAR , dinero_t DECIMAL(8,2) , comentario_t VARCHAR ) RETURNS VOID
AS
$$

---
--- VARIABLES
---

DECLARE

	fecha_m		DATE := date_trunc('day',fecha_t);
	fecha_a		DATE := date_trunc('month',fecha_t);
	i_envio		DECIMAL(8,2) := 0.0;
	i_trabajo	DECIMAL(8,2) := 0.0;
	i_otros		DECIMAL(8,2) := 0.0;
	i_total		DECIMAL(8,2) := 0.0;
	m_deposito	DECIMAL(8,2) := 0.0;
	m_retiro	DECIMAL(8,2) := 0.0;
	e_hacienda	DECIMAL(8,2) := 0.0;
	e_alimentos	DECIMAL(8,2) := 0.0;
	e_transporte	DECIMAL(8,2) := 0.0;
	e_necesidad	DECIMAL(8,2) := 0.0;
	e_otros		DECIMAL(8,2) := 0.0;
	e_total		DECIMAL(8,2) := 0.0;
	b_balance	DECIMAL(8,2) := 0.0;
	f_cuenta	DECIMAL(8,2) := 0.0;
	f_efectivo	DECIMAL(8,2) := 0.0;

BEGIN

---
--- 0 - Preparacion
---
	---
	--- 0.1 Crear transcciones
	---

	IF EXISTS ( SELECT * FROM transacciones_mes WHERE fecha = fecha_m )
		THEN
		ELSE
			INSERT INTO transacciones_mes VALUES ( fecha_m , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0);
	END IF;

	IF EXISTS ( SELECT * FROM transacciones_ano WHERE fecha = fecha_a )
		THEN
		ELSE
			INSERT INTO transacciones_ano VALUES ( fecha_a , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0);
	END IF;

	---
	--- 0.2 Preparar datos
	---

	CASE
		WHEN (tipo_t = 'ingreso' AND detalles_t = 'envio')   THEN i_envio = dinero_t;   i_total = dinero_t; b_balance = dinero_t ; f_efectivo = dinero_t;
		WHEN (tipo_t = 'ingreso' AND detalles_t = 'trabajo') THEN i_trabajo = dinero_t; i_total = dinero_t; b_balance = dinero_t ; f_efectivo = dinero_t;
		WHEN (tipo_t = 'ingreso' AND detalles_t = 'otros')   THEN i_otros = dinero_t;   i_total = dinero_t; b_balance = dinero_t ; f_efectivo = dinero_t;
		WHEN (tipo_t = 'movimiento' AND detalles_t = 'deposito') THEN m_deposito = dinero_t; f_cuenta = dinero_t;       f_efectivo = -1 * dinero_t;
		WHEN (tipo_t = 'movimiento' AND detalles_t = 'retiro')   THEN m_retiro = dinero_t;   f_cuenta = -1 * dinero_t;  f_efectivo = dinero_t;
		WHEN (tipo_t = 'egreso' AND detalles_t = 'hacienda')   THEN e_hacienda = dinero_t;   e_total = dinero_t; b_balance = -1 * dinero_t ;  f_efectivo = -1 * dinero_t;
		WHEN (tipo_t = 'egreso' AND detalles_t = 'alimentos')  THEN e_alimentos = dinero_t;  e_total = dinero_t; b_balance = -1 * dinero_t ;  f_efectivo = -1 * dinero_t;
		WHEN (tipo_t = 'egreso' AND detalles_t = 'transporte') THEN e_transporte = dinero_t; e_total = dinero_t; b_balance = -1 * dinero_t ;  f_efectivo = -1 * dinero_t;
		WHEN (tipo_t = 'egreso' AND detalles_t = 'necesidad')  THEN e_necesidad = dinero_t;  e_total = dinero_t; b_balance = -1 * dinero_t ;  f_efectivo = -1 * dinero_t;
		WHEN (tipo_t = 'egreso' AND detalles_t = 'otros')      THEN e_otros = dinero_t;      e_total = dinero_t; b_balance = -1 * dinero_t ;  f_efectivo = -1 * dinero_t;
	ELSE
	
	END CASE;

---
--- 1 - Actualizar - transacciones_dia
---

	INSERT INTO transacciones_dia VALUES ( fecha_t , tipo_t , detalles_t , dinero_t , comentario_t );

---
--- 2 - Actualizar - transacciones_mes
---

	UPDATE transacciones_mes SET ingreso_envio   = ingreso_envio   + i_envio ,
	       			     ingreso_trabajo = ingreso_trabajo + i_trabajo ,
				     ingreso_otros   = ingreso_otros   + i_otros ,
				     ingreso_total   = ingreso_total   + i_total ,
	       			     movimiento_deposito = movimiento_deposito + m_deposito ,
				     movimiento_retiro   = movimiento_retiro   + m_retiro ,
				     egreso_hacienda   = egreso_hacienda   + e_hacienda ,
				     egreso_alimentos  = egreso_alimentos  + e_alimentos ,
				     egreso_transporte = egreso_transporte + e_transporte ,
				     egreso_necesidad  = egreso_necesidad  + e_necesidad ,
				     egreso_otros      = egreso_otros      + e_otros ,
				     egreso_total      = egreso_total      + e_total ,
				     balance = balance + b_balance
	WHERE fecha = fecha_m;

---
--- 3 - Actualizar - transacciones_ano
---

	UPDATE transacciones_ano SET ingreso_envio   = ingreso_envio   + i_envio ,
	       			     ingreso_trabajo = ingreso_trabajo + i_trabajo ,
				     ingreso_otros   = ingreso_otros   + i_otros ,
				     ingreso_total   = ingreso_total   + i_total ,
	       			     movimiento_deposito = movimiento_deposito + m_deposito ,
				     movimiento_retiro   = movimiento_retiro   + m_retiro ,
				     egreso_hacienda   = egreso_hacienda   + e_hacienda ,
				     egreso_alimentos  = egreso_alimentos  + e_alimentos ,
				     egreso_transporte = egreso_transporte + e_transporte ,
				     egreso_necesidad  = egreso_necesidad  + e_necesidad ,
				     egreso_otros      = egreso_otros      + e_otros ,
				     egreso_total      = egreso_total      + e_total ,
				     balance = balance + b_balance
	WHERE fecha = fecha_a;

---
--- 4 - Actualizar - dinero
---

	UPDATE dinero SET dinero = dinero + f_cuenta  WHERE tipo = 'cuenta_bancaria';
	UPDATE dinero SET dinero = dinero + f_efectivo WHERE tipo = 'efectivo';

END;
$$
LANGUAGE PLPGSQL;


/*
ingreso
	envio
	trabajo
	otros
movimiento
	deposito
	retiro
egreso
	hacienda
	alimentos
	transporte
	necesidad
	otros
balance
*/
