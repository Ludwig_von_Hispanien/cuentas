#ifndef __FECHA__HH__
#define __FECHA__HH__

#include <stdio.h>
#include <ctime>


class Fecha
{
public:
  Fecha();
  virtual ~Fecha();

  int getFecha();

  int getDia();
  int getMes();
  int getAno();
  
protected:
  tm *fecha;
  
};

Fecha::Fecha()
{
  time_t hoy = time(NULL);
  fecha = localtime(&hoy);
}

Fecha::~Fecha()
{
  delete fecha;
}

int Fecha::getFecha()
{
  return fecha->tm_mday + ((fecha->tm_mon + 1) * 100) + ((fecha->tm_mon + 1900) * 10000);
}

int Fecha::getDia()
{
  return fecha->tm_mday;
}

int Fecha::getMes()
{
  return fecha->tm_mon + 1;
}

int Fecha::getAno()
{
  return fecha->tm_year + 1900;
}


#endif
