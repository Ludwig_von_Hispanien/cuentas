#include <stdio.h>
#include <string>

#include "Cuentas.hh"


int main(int argc, char** argv)
{

  
  
  if(argc==1)
    {
      Cuentas<float,std::string> cuentas;
      cuentas.iniciar();
      system("clear");
    }
  else
    {

      bool val = false;
 
      if( (strcmp(argv[1],"-n")==0) || (strcmp(argv[1],"--new")==0) )
	{
	  system("sudo -u postgres bash -c \"psql -c \\\"CREATE USER cuentas_usuario WITH PASSWORD 'cuentaspass';\\\"\"");
	  system("sudo -u postgres bash -c \"createdb cuentas\"");
	  system("sudo -u postgres bash -c \"psql -c \\\"\\i /etc/cuentas/cuentas.sql;\\\"\"");

	  printf("Create user and database.\n\n");
	  val = true;
	}
      if( (strcmp(argv[1],"-d")==0) || (strcmp(argv[1],"--delete")==0) )
	{
	  system("sudo -u postgres bash -c \"psql -c \\\"DROP DATABASE cuentas;\\\"\"");
	  system("sudo -u postgres bash -c \"psql -c \\\"DROP USER cuentas_usuario;\\\"\"");

	  printf("Delete user and database. \n\n");
	  val = true;
	}
      if( (strcmp(argv[1],"-b")==0) || (strcmp(argv[1],"--backup")==0) )
	{
	  system("mkdir -p $HOME/.config/cuentas");
	  system("PGPASSWORD=\"cuentaspass\" pg_dump -U cuentas_usuario -h localhost cuentas > $HOME/.config/cuentas/cuentas_backup.sql");
	  
	  printf("Creates cuentas backup. \n\n");
	  val = true;
	}
      if( (strcmp(argv[1],"-r")==0) || (strcmp(argv[1],"--restore")==0) )
	{
	  system("sudo -u postgres bash -c \"psql -c \\\"CREATE USER cuentas_usuario WITH PASSWORD 'cuentaspass';\\\"\"");
	  system("sudo -u postgres bash -c \"createdb cuentas\"");
	  system("sudo cp $HOME/.config/cuentas/cuentas_backup.sql /etc/cuentas/");
	  system("sudo -u postgres bash -c \"PGPASSWORD=\"cuentaspass\" psql -U cuentas_usuario -h localhost cuentas < /etc/cuentas/cuentas_backup.sql\"");
	  
	  printf("Restore cuentas backup. \n\n");
	  val = true;
	}

      if( val == false )
	{
	  if( !((strcmp(argv[1],"-h")==0) || (strcmp(argv[1],"--help")==0)) )
	    printf("ERROR. Invalid argument...\n\n");

	  printf("Usage: cuentas [OPTION]\n\r"
		 "  -b, --backup    Create the database backup.\n\r"
		 "  -d, --delete    Delete the database.\n\r"
		 "  -n, --new       Create the database.\n\r"
		 "  -r, --restore   Restore the database.\n\r");
	}  
  
    }
 
  return 0;

}
