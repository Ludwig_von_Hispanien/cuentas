# cuentas - simples admin of money in spanish

include config.mk

SRC = cuentas.cpp
OBJ = ${SRC:.cpp=.o}

all: cuentas

.cpp.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: BaseDeDatos.hh Cuentas.hh Fecha.hh Grafica.hh

cuentas: ${OBJ}
	${CC} -o $@ ${OBJ} ${CFLAGS}

clean:
	rm -f cuentas ${OBJ} cuentas-${VERSION}.tar.gz

dist: clean
	mkdir -p cuentas-${VERSION}
	cp -R LICENSE Makefile README.md config.mk cuentas.sql cuentas.1 BaseDeDatos.hh Cuentas.hh Fecha.hh Grafica.hh ${SRC} cuentas-${VERSION}
	tar -cf cuentas-${VERSION}.tar cuentas-${VERSION}
	gzip cuentas-${VERSION}.tar
	rm -rf cuentas-${VERSION}

install: all
	mkdir -p ${SCRIPTSDIR}
	cp -f cuentas ${PREFIX}/bin
	chmod 755 ${PREFIX}/bin/cuentas
	mkdir -p  ${MANPREFIX}/man1
	sed "s/VERSION/${VERSION}/g" < cuentas.1 > ${DESTDIR}${MANPREFIX}/man1/cuentas.1
	cp -f cuentas.sql ${SCRIPTSDIR}

uninstall:
	rm -r ${SCRIPTSDIR} ${PREFIX}/bin/cuentas

.PHONY: all clean dist install uninstall
