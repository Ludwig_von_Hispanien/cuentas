#ifndef __CUENTAS__HH__
#define __CUENTAS__HH__


#include <pqxx/pqxx>

#include "Grafica.hh"
#include "BaseDeDatos.hh"


template<typename T, typename U>
class Cuentas{
public:
  Cuentas();
  virtual ~Cuentas();

  void iniciar();
  
protected:

  Grafica *grafica;
  BaseDeDatos *datos;
  
  void actual();
  void revisar();
  void dinero();
  void ingresar();
  void dia();
  void mes();
  void ano();
  void dia_revisar();
  void mes_revisar();
  void ano_revisar();
  
};

template<typename T, typename U>
Cuentas<T,U>::Cuentas()
{
  grafica = new Grafica();
  datos = new BaseDeDatos();
}

template<typename T, typename U>
Cuentas<T,U>::~Cuentas()
{
  delete grafica;
  delete datos;
}

template<typename T, typename U>
void Cuentas<T,U>::iniciar()
{
  
  do{
    
    int nau;
    nau = grafica->principal();

    switch(nau)
      {
      case 1:
	actual();
	break;
      case 2:
	revisar();
	break;
      case 3:
	return;	
      }
    
  }while(true);
  
}

template<typename T, typename U>
void Cuentas<T,U>::actual()
{
  
  do{
    
    int nau;
    nau = grafica->actual();

    switch(nau)
      {
      case 1:
	dinero();
	break;
      case 2:
	ingresar();
	break;
      case 3:
	dia();
	break;
      case 4:
	mes();
	break;
      case 5:
	ano();
	break;
      case 6:
	return;
      }
    
  }while(true);
  
}

template<typename T, typename U>
void Cuentas<T,U>::revisar()
{
  
  do{
    
    int nau;
    nau = grafica->revisar();

    switch(nau)
      {
      case 1:
	dia_revisar();
	break;
      case 2:
	mes_revisar();
	break;
      case 3:
	ano_revisar();
	break;
      case 4:
	return;
      }
    
  }while(true);
  
}

template<typename T, typename U>
void Cuentas<T,U>::dinero()
{

  grafica->dinero( datos->dinero() );
  
}

template<typename T, typename U>
void Cuentas<T,U>::ingresar()
{

  std::string sig;

  sig = grafica->ingresar();

  if(sig != "-")
    datos->ingresar(sig);
  
}

template<typename T, typename U>
void Cuentas<T,U>::dia()
{

  grafica->dia( datos->dia() );
  
}

template<typename T, typename U>
void Cuentas<T,U>::mes()
{

  grafica->mes( datos->mes() );
  
}

template<typename T, typename U>
void Cuentas<T,U>::ano()
{

  grafica->ano( datos->ano() );
  
}

template<typename T, typename U>
void Cuentas<T,U>::dia_revisar()
{

  pqxx::result nau;
  std::string sig;
  
  sig = grafica->dia_revisar();

  if(sig!="-")
    grafica->dia( datos->dia(sig) );
  
}

template<typename T, typename U>
void Cuentas<T,U>::mes_revisar()
{

  pqxx::result nau;
  std::string sig;
  
  sig = grafica->mes_revisar();

  if(sig!="-")
    grafica->mes( datos->mes(sig));
   
}

template<typename T, typename U>
void Cuentas<T,U>::ano_revisar()
{

  pqxx::result nau;
  std::string sig;
  
  sig = grafica->ano_revisar();

  if(sig!="-")
    grafica->ano( datos->ano(sig));
  
}


#endif
