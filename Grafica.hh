#ifndef __GRAFICA__HH__
#define __GRAFICA__HH__


#include <stdio.h>
#include <iostream>
#include <pqxx/pqxx>


class Grafica{
public:
  Grafica();
  virtual ~Grafica();
  
  int principal();
  int actual();
  int revisar();

  std::string ingresar();

  void dinero(pqxx::result nau);
  
  void dia(pqxx::result nau);
  void mes(pqxx::result nau);
  void ano(pqxx::result nau);
  std::string dia_revisar();
  std::string mes_revisar();
  std::string ano_revisar();
  
};

Grafica::Grafica()
{
}

Grafica::~Grafica()
{
}

int Grafica::principal()
{

  int nau = 0;
  
  do{

    system("clear");
    
    printf("\t\t\t\t\t\t\t\t\tCONTROL DE CUENTAS FINANCIERAS\n\n\n"
	   "Opciones: \n\n"
	   "1.Cuentas Presentes.\n"
	   "2.Revisar Cuentas.\n"
	   "3.Salir\n\n"
	   "Seleciones una opción: ");
    scanf("%d",&nau);
    while ((getchar()) != '\n');

    switch(nau)
      {
      case 1: case 2: case 3:
	return nau;
      }
   
  }while(true);
  
}

int Grafica::actual()
{

  int nau = 0;
  
  do{

    system("clear");
    
    printf("\t\t\t\t\t\t\t\tCONTROL DE CUENTAS FINANCIERAS ACTUALES\n\n\n"
	   "Opciones: \n\n"
	   "1.Consulta del saldo actual.\n"
	   "2.Ingresar nueva transacción.\n"
	   "3.Revisar ultimas transcciones del mes por dia.\n"
	   "4.Resumen de las transacciones del mes por dia.\n"
	   "5.Resumen de las transacciones del ano por mes.\n"
	   "6.Salir\n\n"
	   "Seleciones una opción: ");
    scanf("%d",&nau);
    while ((getchar()) != '\n');

    switch(nau)
      {
      case 1: case 2: case 3: case 4: case 5: case 6:
	return nau;
      }
   
  }while(true);
  
}

int Grafica::revisar()
{

  int nau = 0;
  
  do{

    system("clear");
    
    printf("\t\t\t\t\t\t\t\t\tREVISION DE CUENTAS FINANCIERAS\n\n\n"
	   "Opciones: \n\n"
	   "1.Resumen de las transacciones de un mes por transacción.\n"
	   "2.Resumen de las transacciones de un mes por dia.\n"
	   "3.Resumen de las transacciones de un ano por mes.\n"
	   "4.Salir\n\n"
	   "Seleciones una opción: ");
    scanf("%d",&nau);
    while ((getchar()) != '\n');

    switch(nau)
      {
      case 1: case 2: case 3: case 4:
	return nau;
      }
   
  }while(true);
  
}

std::string Grafica::ingresar()
{

  int nau[3];
  
  system("clear");
  printf("\t\t\t\t\t\t\t\t\tINGRESO DE UNA NUEVA TRANSACCION\n\n\n"
	 "Selecciones la operacion a realizar:\n\n"
	 "1.Ingreso\n"
	 "2.Movimiento\n"
	 "3.Egreso\n\n"
	 "Opcion: ");
  scanf("%d",&nau[0]);
  while ((getchar()) != '\n');

  printf("\n\nSeleccione la naturaleza de la operacion: \n\n");
  
  switch(nau[0])
    {
    case 1:
      printf("1.Envio\n"
	     "2.Trabajo\n"
	     "3.Otros\n");
      break;
    case 2:
      printf("1.Deposito\n"
	     "2.Retiro\n");
      break;
    case 3:
      printf("1.Hacienda\n"
	     "2.Alimentos\n"
	     "3.Transporte\n"
	     "4.Necesidad\n"
	     "5.Otros\n");
      break;
    default:
      printf("Opcion no valida...\n");
      scanf("%d",&nau[0]);
      while ((getchar()) != '\n');
      return "-";

    }

  printf("\nOpcion: ");
  scanf("%d",&nau[1]);
  while ((getchar()) != '\n');

  float kai;
  
  printf("\n\nIngrese la cantidad: ");
  scanf("%f",&kai);
  while ((getchar()) != '\n');

  if(kai<=0.0)
    {
      printf("\nValor no valido...\n");
      scanf("%d",&nau[0]);
      while ((getchar()) != '\n');
      return "-";
    }
  
  std::string sig,fra0,fra1;
  
  if(  ( (nau[0]==1)&&(nau[1]==3) )  ||  ( (nau[0]==3)&&(nau[1]==5) )  )
    {
      printf("\n\nIngrese una palabra a modo de complemento de la transaccion: ");
      scanf("%s",&sig[0]);
      while ((getchar()) != '\n');
    }
  else
    sig = "-";

  switch(nau[0])
    {
    case 1:
      fra0 = "ingreso";
      switch(nau[1])
	{
	case 1:
	  fra1 = "envio";
	  break;
	case 2:
	  fra1 = "trabajo";
	  break;
	case 3:
	  fra1 = "otros";
	  break;
	default:
	  printf("Opcion no valida...\n");
	  scanf("%d",&nau[0]);
	  while ((getchar()) != '\n');
	  return "-";
	}
      break;
      
    case 2:
      fra0 = "movimiento";
      switch(nau[1])
	{
	case 1:
	  fra1 = "deposito";
	  break;
	case 2:
	  fra1 = "retiro";
	  break;
	default:
	  printf("Opcion no valida...\n");
	  scanf("%d",&nau[0]);
	  while ((getchar()) != '\n');
	  return "-";
	}
      break;

    case 3:
      fra0 = "egreso";
      switch(nau[1])
	{
	case 1:
	  fra1 = "hacienda";
	  break;
	case 2:
	  fra1 = "alimentos";
	  break;
	case 3:
	  fra1 = "transporte";
	  break;
	case 4:
	  fra1 = "necesidad";
	  break;
	case 5:
	  fra1 = "otros";
	  break;
	default:
	  printf("Opcion no valida...\n");
	  scanf("%d",&nau[0]);
	  while ((getchar()) != '\n');
	  return "-";
	}
      break;

    default:
      printf("Opcion no valida...\n");
      scanf("%d",&nau[0]);
      while ((getchar()) != '\n');
      return "-";

    }

  printf("\n\nConfirme la veracidad de los datos: %s - %s - %.2f",fra0.c_str(),fra1.c_str(),kai);

  if(  ( (nau[0]==1)&&(nau[1]==3) )  ||  ( (nau[0]==3)&&(nau[1]==5) )  )
    printf(" - %s",sig.c_str());

  char dec;
  printf("\n\n¿Los datos son correctos? <s/n> : ");
  scanf("%c",&dec);
  while ((getchar()) != '\n');

  if(dec == 's')
    {
      sig = "'" + fra0 + "','" + fra1 + "','" + std::to_string(kai) + "','" + sig.c_str() +"'";
      return sig;
    }

  return "-";
  
}


void Grafica::dinero(pqxx::result nau)
{

  system("clear");
  printf("     Tipo          |       Cantidad\n"
	 "-------------------+----------------------\n"
	 "                   |\n");
  
  std::string kai = "";

  for( int i = 0; i<nau.size() ; i++ )
    {
      kai = nau[i][0].as<std::string>();
      printf("  %-15s  |      ",kai.c_str());
      kai = nau[i][1].as<std::string>();
      printf("%10s\n",kai.c_str());
    }

  printf("                   |\n");
  
  printf("\n\n\nIngrese <s> para continuar...\n");
  char opc;
  scanf("%c",&opc);
  while ((getchar()) != '\n');
  
}

void Grafica::dia(pqxx::result nau)
{

  system("clear");
  printf("    Fecha     |    Tipo    |       Detalles       |  Cantidad  |        Comentarios\n"
	 "--------------+------------+----------------------+------------+---------------------------\n");

  std::string kai = "";

  for( int i = 0; i<nau.size() ; i++ )
    {
      kai = nau[i][0].as<std::string>();
      printf("  %s  | ",kai.c_str());
      kai = nau[i][1].as<std::string>();
      printf("%-10s |    ",kai.c_str());
      kai = nau[i][2].as<std::string>();
      printf("%-15s   |  ",kai.c_str());
      kai = nau[i][3].as<std::string>();
      printf("%8s  |  ",kai.c_str());
      kai = nau[i][4].as<std::string>();
      printf("%20s\n",kai.c_str());
    }

  printf("\n\n\nIngrese <s> para continuar...\n");
  char opc;
  scanf("%c",&opc);
  while ((getchar()) != '\n');
  
}

void Grafica::mes(pqxx::result nau)
{

  system("clear");
  printf("            |                  Ingreso                  |      Movimiento     |                                    Egreso                                   |\n"
	 "   Fecha    +----------+----------+----------+----------+----------+----------+------------+------------+------------+------------+------------+------------+   Saldo\n"
	 "            |  Envio   | Trabajo  |  Otros   |  Total   | Deposito |  Retiro  |  Hacienda  | Alimentos  | Transporte | Necesidad  |   Otros    |   Total    |\n"
	 "------------+----------+----------+----------+----------+----------+----------+------------+------------+------------+------------+------------+------------+------------\n");

  printf("            |");
  for( int i = 0 ; i<6 ; i++ )
    printf("          |");
  for( int i = 0 ; i<6 ; i++ )
    printf("            |");
  printf("\n");
  
  std::string kai = "";
  
  for( int i = 0 ; i<nau.size() ; i++ )
    {
      kai = nau[i][0].as<std::string>();
      kai = kai.substr(0,10);
      printf(" %10s | ",kai.c_str());
      kai = nau[i][1].as<std::string>();
      printf("%8s | ",kai.c_str());
      kai = nau[i][2].as<std::string>();
      printf("%8s | ",kai.c_str());
      kai = nau[i][3].as<std::string>();
      printf("%8s | ",kai.c_str());
      kai = nau[i][4].as<std::string>();
      printf("%8s | ",kai.c_str());
      kai = nau[i][5].as<std::string>();
      printf("%8s | ",kai.c_str());
      kai = nau[i][6].as<std::string>();
      printf("%8s |  ",kai.c_str());
      kai = nau[i][7].as<std::string>();
      printf("%8s  |  ",kai.c_str());
      kai = nau[i][8].as<std::string>();
      printf("%8s  |  ",kai.c_str());
      kai = nau[i][9].as<std::string>();
      printf("%8s  |  ",kai.c_str());
      kai = nau[i][10].as<std::string>();
      printf("%8s  |  ",kai.c_str());
      kai = nau[i][11].as<std::string>();
      printf("%8s  |  ",kai.c_str());
      kai = nau[i][12].as<std::string>();
      printf("%8s  |  ",kai.c_str());
      kai = nau[i][13].as<std::string>();
      printf("%8s\n",kai.c_str());
    }

  printf("            |");
  for( int i = 0 ; i<6 ; i++ )
    printf("          |");
  for( int i = 0 ; i<6 ; i++ )
    printf("            |");
  
  printf("\n\n\nIngrese <s> para continuar...\n");
  char opc;
  scanf("%c",&opc);
  while ((getchar()) != '\n');
  
}

void Grafica::ano(pqxx::result nau)
{
  
  system("clear");
  printf("         |                  Ingreso                  |      Movimiento     |                                    Egreso                                   |\n"
	 "  Fecha  +----------+----------+----------+----------+----------+----------+------------+------------+------------+------------+------------+------------+   Saldo\n"
	 "         |  Envio   | Trabajo  |  Otros   |  Total   | Deposito |  Retiro  |  Hacienda  | Alimentos  | Transporte | Necesidad  |   Otros    |   Total    |\n"
	 "---------+----------+----------+----------+----------+----------+----------+------------+------------+------------+------------+------------+------------+------------\n");

  printf("         |");
  for( int i = 0 ; i<6 ; i++ )
    printf("          |");
  for( int i = 0 ; i<6 ; i++ )
    printf("            |");
  printf("\n");
  
  std::string kai = "";
  
  for( int i = 0 ; i<nau.size() ; i++ )
    {
      kai = nau[i][0].as<std::string>();
      kai = kai.substr(0,7);
      printf(" %7s | ",kai.c_str());
      kai = nau[i][1].as<std::string>();
      printf("%8s | ",kai.c_str());
      kai = nau[i][2].as<std::string>();
      printf("%8s | ",kai.c_str());
      kai = nau[i][3].as<std::string>();
      printf("%8s | ",kai.c_str());
      kai = nau[i][4].as<std::string>();
      printf("%8s | ",kai.c_str());
      kai = nau[i][5].as<std::string>();
      printf("%8s | ",kai.c_str());
      kai = nau[i][6].as<std::string>();
      printf("%8s |  ",kai.c_str());
      kai = nau[i][7].as<std::string>();
      printf("%8s  |  ",kai.c_str());
      kai = nau[i][8].as<std::string>();
      printf("%8s  |  ",kai.c_str());
      kai = nau[i][9].as<std::string>();
      printf("%8s  |  ",kai.c_str());
      kai = nau[i][10].as<std::string>();
      printf("%8s  |  ",kai.c_str());
      kai = nau[i][11].as<std::string>();
      printf("%8s  |  ",kai.c_str());
      kai = nau[i][12].as<std::string>();
      printf("%8s  |  ",kai.c_str());
      kai = nau[i][13].as<std::string>();
      printf("%8s\n",kai.c_str());
    }

  printf("         |");
  for( int i = 0 ; i<6 ; i++ )
    printf("          |");
  for( int i = 0 ; i<6 ; i++ )
    printf("            |");
  
  printf("\n\n\nIngrese <s> para continuar...\n");
  char opc;
  scanf("%c",&opc);
  while ((getchar()) != '\n');
  
}

std::string Grafica::dia_revisar()
{

  system("clear");

  int fecha[3];
  
  printf("Ingrese el año del registro: ");
  scanf("%d",&fecha[0]);
  while ((getchar()) != '\n');

  if( fecha[0]<=2019 )
    {
      printf("\nDato no valido.\n\nIngrese una tecla para continuar...");
      char opc;
      scanf("%c",&opc);
      return "-";
    }
  
  printf("Ingrese el mes del registro: ");
  scanf("%d",&fecha[1]);
  while ((getchar()) != '\n');

  if( (fecha[1]<1) || (12<fecha[1])  )
    {
      printf("\nDato no valido.\n\nIngrese una tecla para continuar...");
      char opc;
      scanf("%c",&opc);
      return "-";
    }
  
  printf("Ingrese el dia del registro: ");
  scanf("%d",&fecha[2]);
  while ((getchar()) != '\n');

  if( (fecha[2]<1) || (31<fecha[2])  )
    {
      printf("\nDato no valido.\n\nIngrese una tecla para continuar...");
      char opc;
      scanf("%c",&opc);
      return "-";
    }

  std::string sig = "";

  sig = "'" + std::to_string(fecha[0]) + "-" + std::to_string(fecha[1]) + "-" + std::to_string(fecha[2]) + "'";

  return sig;
  
}

std::string Grafica::mes_revisar()
{

  system("clear");

  int fecha[2];
  
  printf("Ingrese el año del registro: ");
  scanf("%d",&fecha[0]);
  while ((getchar()) != '\n');

  if( fecha[0]<=2019 )
    {
      printf("\nDato no valido.\n\nIngrese una tecla para continuar...");
      char opc;
      scanf("%c",&opc);
      return "-";
    }
  
  printf("Ingrese el mes del registro: ");
  scanf("%d",&fecha[1]);
  while ((getchar()) != '\n');

  if( (fecha[1]<1) || (12<fecha[1])  )
    {
      printf("\nDato no valido.\n\nIngrese una tecla para continuar...");
      char opc;
      scanf("%c",&opc);
      return "-";
    }

  std::string sig = "";

  sig = "'" + std::to_string(fecha[0]) + "-" + std::to_string(fecha[1]) + "-";

  return sig;
}

std::string Grafica::ano_revisar()
{

  system("clear");

  int fecha;
  
  printf("Ingrese el año del registro: ");
  scanf("%d",&fecha);
  while ((getchar()) != '\n');

  if( fecha<=2019 )
    {
      printf("\nDato no valido.\n\nIngrese una tecla para continuar...");
      char opc;
      scanf("%c",&opc);
      return "-";
    }

  std::string sig = "";

  sig = "'" + std::to_string(fecha) + "-";

  return sig;
}


#endif
