# Cuentas

Cuentas is a very simples money admin in spanish writing in c++ and sql.

## Requirements
In order to build Cuentas you need the library pqxx, postgresql and c++ compiler.

## Installation
Enter the following command to build and install Cuentas.

`make`

`sudo make install`

## Running Cuentas
First you need create a user and a database with the next command:

`cuentas -n`

afterwards runs without -n.

`cuentas`
